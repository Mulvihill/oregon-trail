function Traveler (name, food, isHealthy) {
    this.name = name;
    this.food = food = 1;
    this.isHealthy = isHealthy =>  {
        if (this.food <= 0) {
            return false;
        }
        else {return true};
    };
}
Traveler.prototype = {
    constructor: Traveler(this.name),

    hunt: function () {
        this.food += 2;
        return this.food;
    },

    eat: function () {
        if (this.food <= 0) {
            this.isHealthy = false;
        }
        else {
            this.food -= 1;
            return this.food;
        }
    }
    
    
    
}


function Wagon (capacity, passengers) {
    this.capacity = capacity;
    this.passengers = passengers = [];
}

Wagon.prototype = {
    constructor: Wagon(this.capacity),

    getAvailableSeatCount: function() {
        x = this.capacity - this.passengers.length;
        return x;
    },

    join: function(person) {
        if (this.getAvailableSeatCount() > 0) {
            this.passengers.push(person);
        }
        else {return "There is no room!"}
    },

    shouldQuarantine: function () {
        y = 0;
        for (tr in this.passengers) {
            if(this.passengers[tr].isHealthy === false) {
                return true;
            }
            else if (this.passengers[tr] == this.passengers.length - 1 && this.passengers[tr].isHealthy() === true ) {
                return false;
            }
        }
    },

    totalFood: function () {
        f = 0;
        for (tr in this.passengers) {
            f += this.passengers[tr].food;
        }
        return f;
    }
}


// Create a wagon that can hold 2 people
let wagon = new Wagon(2);
// Create three travelers
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');
console.log(`${wagon.getAvailableSeatCount()} should be 2`);
wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);
wagon.join(juan);
wagon.join(maude); // There isn't room for her!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);
henrietta.hunt(); // get more food
juan.eat();
juan.eat(); // juan is now hungry (sick)
console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);